﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    public class Account
    {
        public Account(DateTime openDate, decimal amount, Guid ownewUserId)
        {
            Id = Guid.NewGuid();
            OpenDate = openDate;
            Amount = amount;
            OwnewUserId = ownewUserId;
        }

        public Guid Id { get; set; }
        public DateTime OpenDate { get; set; }
        public decimal Amount { get; set; }
        public Guid OwnewUserId { get; set; }

        public History DoOperation(DateTime dateTime, OperationType operType, decimal amount)
        {
            switch (operType)
            {
                case OperationType.Пополнение:
                    this.Amount += amount;
                    break;
                case OperationType.Снятие:
                    this.Amount -= amount;
                    break;
                default:
                    break;
            }
            return new History(dateTime, operType, amount, this.Id);
        }

        public override string ToString()
        {
            return $"Дата открытия: {OpenDate.ToShortDateString()}, сумма: {Amount}";
        }
    }

}
