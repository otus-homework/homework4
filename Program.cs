﻿using System;

namespace Homework4
{
    class Program
    {
        static void Main(string[] args)
        {
            Bank bank = new Bank();


            Console.WriteLine("1.Вывод информации о заданном аккаунте по логину и паролю");
            var user = bank.GetUser("ivanov", "123456");
            Console.WriteLine(user.ToString() ?? "Неверен логин или пароль!");
            Console.WriteLine(new string('-', 30));

            if (user != null)
            {
                Console.WriteLine("2.Вывод данных о всех счетах заданного пользователя");
                foreach (var acc in bank.GetUserAccounts(user))
                    Console.WriteLine(acc);
                Console.WriteLine(new string('-', 30));

                Console.WriteLine("3.Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту");
                foreach (var pair in bank.GetUserAccountsWithHistories(user))
                {
                    Console.WriteLine(pair.Key);
                    foreach (var item in pair.Value)
                        Console.WriteLine(item);
                }
                Console.WriteLine(new string('-', 30));
            }

            Console.WriteLine("4.Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта");
            foreach (var item in bank.GetRefillHistoriesWithUsers())
                Console.WriteLine($"{item.Item1.ToString()} {item.Item2.GetFIO()}");
            Console.WriteLine(new string('-', 30));

            Console.WriteLine("5.Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой)");
            foreach (var item in bank.GetUsersAmountIsGreaterThanNumber(5000))
                Console.WriteLine(item);

            Console.ReadKey();

        }
    }
}
