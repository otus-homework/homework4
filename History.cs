﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    public class History
    {
        public History(DateTime operationDate, OperationType operType, decimal amount, Guid accountId)
        {
            Id = Guid.NewGuid();
            OperationDate = operationDate;
            OperType = operType;
            Amount = amount;
            AccountId = accountId;
        }

        public Guid Id { get; }
        public DateTime OperationDate { get; set; }
        public OperationType OperType { get; set; }
        public decimal Amount { get; set; }
        public Guid AccountId { get; set; }

        public override string ToString()
        {
            return $"Дата: {OperationDate.ToString()}, тип: {OperationType.Пополнение.ToString()}, сумма: {Amount}";
        }
    }
}
