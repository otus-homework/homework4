﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Homework4
{
    public class Bank
    {
        List<User> users;
        List<Account> accounts;
        List<History> histories;

        public Bank()
        {
            Initialization();
        }

        /// <summary>
        /// Инициализация тестовых примеров
        /// </summary>
        private void Initialization()
        {

            User user1 = new User("Иван", "Иванов", "Иванович", "111-111", "1", DateTime.Parse("2019-12-01"), "ivanov", "123456");
            User user2 = new User("Петр", "Петров", "Петрович", "222-222", "2", DateTime.Parse("2019-11-01"), "petrov", "654321");
            User user3 = new User("Михаил", "Михаилов", "Михаилович", "333-333", "3", DateTime.Parse("2019-10-01"), "mihailov", "qwerty");
            User user4 = new User("Сергей", "Сергеев", "Сергеевич", "444-444", "4", DateTime.Parse("2019-09-01"), "sergeev", "ytrewq");
            users =
                new List<User>()
                {
                    user1,
                    user2,
                    user3,
                    user4
                };

            Account account11 = user1.OpenAccount(DateTime.Parse("2019-12-01"), 1000);
            Account account12 = user1.OpenAccount(DateTime.Parse("2019-12-03"), 2000);
            Account account21 = user2.OpenAccount(DateTime.Parse("2019-11-01"), 3000);
            Account account22 = user2.OpenAccount(DateTime.Parse("2019-11-10"), 4000);
            Account account31 = user3.OpenAccount(DateTime.Parse("2019-10-01"), 5000);
            Account account32 = user3.OpenAccount(DateTime.Parse("2019-10-15"), 6000);
            Account account41 = user4.OpenAccount(DateTime.Parse("2019-09-01"), 7000);
            Account account42 = user4.OpenAccount(DateTime.Parse("2019-09-12"), 8000);
            accounts =
                new List<Account>()
                {
                    account11,
                    account12,
                    account21,
                    account22,
                    account31,
                    account32,
                    account41,
                    account42
                };

            histories =
                new List<History>()
                {
                    account11.DoOperation(DateTime.Parse("2019-12-01"), OperationType.Пополнение, 1000),
                    account11.DoOperation(DateTime.Parse("2019-12-01"), OperationType.Снятие, 1500),
                    account11.DoOperation(DateTime.Parse("2019-12-01"), OperationType.Пополнение, 1750),
                    account12.DoOperation(DateTime.Parse("2019-12-01"), OperationType.Снятие, 1000),
                    account12.DoOperation(DateTime.Parse("2019-12-01"), OperationType.Пополнение, 300),
                    account21.DoOperation(DateTime.Parse("2019-12-01"), OperationType.Пополнение, 1000),
                    account22.DoOperation(DateTime.Parse("2019-12-01"), OperationType.Снятие, 1000),
                    account22.DoOperation(DateTime.Parse("2019-12-01"), OperationType.Снятие, 1000),
                    account22.DoOperation(DateTime.Parse("2019-12-01"), OperationType.Пополнение, 1000),
                    account31.DoOperation(DateTime.Parse("2019-12-01"), OperationType.Пополнение, 1200),
                    account31.DoOperation(DateTime.Parse("2019-12-01"), OperationType.Пополнение, 1000),
                    account32.DoOperation(DateTime.Parse("2019-12-01"), OperationType.Снятие, 1400),
                    account41.DoOperation(DateTime.Parse("2019-12-01"), OperationType.Пополнение, 1000),
                    account41.DoOperation(DateTime.Parse("2019-12-01"), OperationType.Пополнение, 2000),
                    account41.DoOperation(DateTime.Parse("2019-12-01"), OperationType.Пополнение, 1000),
                };
        }

        public User GetUser(string login, string password)
        {
            return users.FirstOrDefault(x => x.Login == login && x.Password == password);
        }

        public IEnumerable<Account> GetUserAccounts(User user)
        {
            return accounts.Where(x => x.OwnewUserId == user.Id);
        }

        public Dictionary<Account, IEnumerable<History>> GetUserAccountsWithHistories(User user)
        {
            return GetUserAccounts(user)
                .Join(
                    histories,
                    a => a.Id,
                    h => h.AccountId,
                    (acc, hist) => new { acc, hist })
                .GroupBy(x => x.acc)
                .ToDictionary(x => x.Key, x => x.Select(y => y.hist));
        }

        public IEnumerable<Tuple<History, User>> GetRefillHistoriesWithUsers()
        {
            return histories
               .Where(x => x.OperType == OperationType.Пополнение)
               .Join(
                   accounts,
                   h => h.AccountId,
                   a => a.Id,
                   (history, account) => new { account.OwnewUserId, history })
               .Join(
                   users,
                   histacc => histacc.OwnewUserId,
                   user => user.Id,
                   (histacc, user) => new Tuple<History, User>(histacc.history, user));
        }

        public IEnumerable<User> GetUsersAmountIsGreaterThanNumber(decimal number)
        {
            return users
               .Join(
                   accounts,
                   user => user.Id,
                   acc => acc.OwnewUserId,
                   (user, acc) => new { user, acc.Amount })
               .Where(x => x.Amount > number)
               .Select(x => x.user)
               .Distinct();
        }
    }
}
