﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework4
{
    public class User
    {
        public User(string firstName, string lastName, string middleName, string phoneNumber, string pasportNumber, DateTime regDate, string login, string password)
        {
            Id = Guid.NewGuid();
            FirstName = firstName;
            LastName = lastName;
            MiddleName = middleName;
            PhoneNumber = phoneNumber;
            PasportNumber = pasportNumber;
            RegDate = regDate;
            Login = login;
            Password = password;
        }

        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PhoneNumber { get; set; }
        public string PasportNumber { get; set; }
        public DateTime RegDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public Account OpenAccount(DateTime dateTime, decimal amount)
        {
            return new Account(dateTime, amount, this.Id);
        }

        public override string ToString()
        {
            return $"{GetFIO()}, тел.: {PhoneNumber}, паспорт: {PasportNumber}, рег.дата: {RegDate.ToShortDateString()}";
        }

        public string GetFIO()
        {
            return $"ФИО: {LastName} {FirstName} {MiddleName}";
        }
    }
}
